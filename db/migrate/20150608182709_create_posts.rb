class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :text, null: false
      t.integer :author_id, null: false, index: true

      t.timestamps null: false
    end
  end
end
