class CreateUserFollows < ActiveRecord::Migration
  def change
    create_table :user_follows do |t|
      t.integer :follower_id, index: true
      t.integer :user_id, index: true

      t.timestamps null: false
    end
  end
end
