class PostLike < ActiveRecord::Base
  belongs_to :user, class_name: 'User', foreign_key: 'liker_id'
  belongs_to :post
end
