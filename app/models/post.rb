class Post < ActiveRecord::Base
  has_many :comments
  belongs_to :author, class_name: 'User', foreign_key: 'author_id'

  has_many :post_likes
  has_many :likers, through: :post_likes, source: :user
end
