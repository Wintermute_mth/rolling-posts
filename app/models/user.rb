class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :created_posts, class_name: 'Post', foreign_key: 'author_id'
  has_many :comments, foreign_key: 'author_id'

  has_many :post_likes
  has_many :liked_posts, through: :post_likes, source: :post

  has_many :user_follows
  has_many :followers, through: :user_follows

  has_many :poster_follows, class_name: 'UserFollow', foreign_key: :follower_id
  has_many :posters, through: :poster_follows, source: :user
end
