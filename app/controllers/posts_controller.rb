class PostsController < ApplicationController

  respond_to :json

  def index
    respond_with Post.all
  end

  def create
    post = Post.new(permitted_params, author: User.first)
    post.author = current_user
    post.save
    respond_with post
  end

  private

    def permitted_params
      params.require(:post).permit(:text)
    end
end