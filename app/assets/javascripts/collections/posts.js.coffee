class RollingPosts.Collections.Posts extends Backbone.Collection

  url: 'api/posts'

  model: RollingPosts.Models.Post
