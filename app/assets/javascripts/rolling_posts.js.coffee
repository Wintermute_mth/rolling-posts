window.RollingPosts =
  Models: {}
  Collections: {}
  Views: {}
  Routers: {}
  initialize: -> 
    new RollingPosts.Routers.Posts()
    Backbone.history.start(pushState: true)

$(document).ready ->
  RollingPosts.initialize()
