class RollingPosts.Routers.Posts extends Backbone.Router
  routes:
    '': 'index'

  initialize: ->
    @collection = new RollingPosts.Collections.Posts()
    @collection.fetch(reset: true)

  index: ->
    view = new RollingPosts.Views.PostsIndex(collection: @collection)
    $('.posts-container').html(view.render().el)
