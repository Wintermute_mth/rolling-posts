class RollingPosts.Views.PostsIndex extends Backbone.View

  template: JST['posts/index']

  events:
    'submit .create-post': 'createPost'

  initialize: ->
    @collection.on('reset', @render, this)
    @collection.on('add', @appendPost)


  render: ->
    $(@el).html(@template(posts: @collection))
    @collection.each @appendPost
    this

  appendPost: (post) =>
    view = new RollingPosts.Views.Post(model: post)
    @$('.posts-list').append(view.render().el)

  createPost: (event) ->
    event.preventDefault()
    attrs = text: $('#post-text').val()
    @collection.create attrs
